import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  alias: DS.attr(),
  order: DS.attr(),

  regions: DS.hasMany('region')
});
