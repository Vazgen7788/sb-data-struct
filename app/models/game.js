import DS from 'ember-data';

export default DS.Model.extend({
  team1_name: DS.attr(),
  team2_name: DS.attr(),
  stats: DS.attr(),
  start_time: DS.attr(),

  competition: DS.belongsTo('competition'),
  events: DS.hasMany('event')
});
