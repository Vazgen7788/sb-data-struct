import DS from 'ember-data';

export default DS.Model.extend({
  market_name: DS.attr(),
  event_name: DS.attr(),
  event_id: DS.attr(),
  market_id: DS.attr(),
  invariant: DS.attr(),
  odd: DS.attr(),

  game: DS.belongsTo('game')
});
