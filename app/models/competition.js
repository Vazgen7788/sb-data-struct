import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  order: DS.attr(),

  region: DS.belongsTo('region'),
  game: DS.hasMany('game')
});
