import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  alias: DS.attr(),
  order: DS.attr(),

  sport: DS.belongsTo('sport'),
  competition: DS.hasMany('competition'),

});
