import Component from '@ember/component';
import getFrameById from 'sp-data-struct/utils/get-frame-by-id';

export default Component.extend({
  classNames: ['monaco-editor'],
  editor: null,
  sourceCode: '',

  init () {
    this._super(...arguments);
    const subscription = event => {
      if (event.source === this.get('frame') && event.data) {
        this.set('sourceCode', event.data.updatedCode);
        if (event.data.updatedCode && this.get('onChange')) {
          this.get('onChange')(event.data.updatedCode);
        }
      }
    };
    this.set('_subscription', subscription);
    window.addEventListener('message', subscription);
  },
  didInsertElement () {
    this._super(...arguments);
    const frame = getFrameById(this.get('elementId'));
    const frameDoc = frame.document;
    this.set('frame', frame);
    frameDoc.open();
    frameDoc.write(`
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" id="print-modal-content">
      <head>
        <script src="/vs/loader.js"></script>
        <script>
          window.require.config({ paths: { 'vs': '/vs' }});
          window.require(['vs/editor/editor.main'], function () {
            if (typeof monaco !== "undefined") {
              let editor = monaco.editor.create(document.getElementById('monaco-editor-wrapper'), {
                value: '${this.get('code') || ""}',
                language: 'javascript'
              });

              let origin = window.location.origin;
              editor.onDidChangeModelContent(function () {
                window.top.postMessage({updatedCode: event.target.value}, origin);
              });
            }
          });
          </script>
      </head>
      <body>
        <div id="monaco-editor-wrapper" style="height:600px;border:1px solid grey"></div>
      </body>
      </html>
    `);
    frame.close();
  },
  willDestroyElement () {
    this._super(...arguments);
    window.removeEventListener('message', this.get('_subscription'));
  }
});
