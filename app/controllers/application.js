import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
export default Controller.extend({
  inject: service('store'),

  init() {
    this._super(...arguments);
    this.set('sports', this.get('store').peekAll('sport'));
  },

  actions: {
    getAllLiveSports() {
      this.get('store').pushPayload({
        data: [
          {
            id: 1,
            type: 'sport',
            attributes: {
              name: 'Soccer',
              alias: 'soccer',
              order: 1,
            }
          },
          {
            id: 2,
            type: 'sport',
            attributes: {
              name: 'Tennis',
              alias: 'tennis',
              order: 2
            },
          },
        ]
      })
    },

    getLiveSport() {
      this.get('store').pushPayload({
        data: [
          {
            id: 1,
            type: 'region',
            attributes: {
              name: 'United Kingdom',
              alias: 'united-kingdom',
              order: 1
            },
            relationships: {
              sport: {
                data: {
                  id: 1,
                  type: 'sport'
                }
              }
            }
          },
        ],
        included: [
          {
            id: 1,
            type: 'competition',
            attributes: {
              name: 'Premier League',
              order: 1
            },
            relationships: {
              region: {
                data: {
                  id: 1,
                  type: 'region'
                }
              }
            }
          },
          {
            id: 1,
            type: 'game',
            attributes: {
              'team1-name': 'Manchester United',
              'team2-name': 'Arsenal',
              order: 1
            },
            relationships: {
              competition: {
                data: {
                  id: 1,
                  type: 'competition'
                }
              }
            }
          }
        ]
      });
    },

    getLiveGame() {
      this.get('store').pushPayload({
        data: {
          id: 2,
          type: 'game',
          attributes: {
            'team1-name': 'Liverpool',
            'team2-name': 'Chelsea',
            order: 2
          },
          relationships: {
            competition: {
              data: {
                id: 1,
                type: 'competition'
              }
            }
          }
        },
        included: [
          {
            id: 1,
            type: 'event',
            attributes: {
              'event-name': 'P1',
              'market-name': '1X2',
              'event-id': '123-987',
              'market-id': '915-987',
              odd: 1.5
            },
            relationships: {
              game: {
                data: {
                  id: 2,
                  type: 'game'
                }
              }
            }
          }
        ]
      });
    },

    getEvent() {
      this.get('store').pushPayload({
        data: {
          id: 2,
          type: 'event',
          attributes: {
            'event-name': 'P2',
            'market-name': '1X2',
            'event-id': '123-984',
            'market-id': '915-985',
            odd: 2.5
          },
          relationships: {
            game: {
              data: {
                id: 2,
                type: 'game'
              }
            }
          }
        }
      });
    }
  }
});
