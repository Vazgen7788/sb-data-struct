import getFrameById from 'sp-data-struct/utils/get-frame-by-id';
import { module, test } from 'qunit';

module('Unit | Utility | get frame by id');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getFrameById();
  assert.ok(result);
});
